// set up  the dependencies

const express = require("express")
const mongoose = require ("mongoose")

// connecting to routes needed
const taskRoute = require("./routes/taskRoute")

// server

const app = express()
const port = 3001
app.use(express.json())
app.use(express.urlencoded({extended: true}))


// database connection
mongoose.connect("mongodb+srv://marcokalalo:admin@wdc028-course-booking.vyczd.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open",()=> console.log(`We're connected to the cloud database`))


// Base URI (Routes)
// allows all the task routes created in the taskRoute.js file to use the "/tasks" route

app.use("/tasks", taskRoute) //http://localhost:3001/tasks



app.listen(port, () => console.log(`Now listening to port ${port}`))