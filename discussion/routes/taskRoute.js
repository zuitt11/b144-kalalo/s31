const express = require("express");
const taskController = require("../controllers/taskController")

// creates a router instance that functions as a middleware and routing system; allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();


// route to get all the tasks

router.get("/", (req,res) =>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})



// create a route for creating a task
router.post("/", (req,res) =>{
	taskController.createTask(req.body).then(result => res.send(result));
})


// delete a specific item
	//: - "wildcard" if tagged with a parameter; helps to recognize a url; the name that comes after the symbol :
		// http://localhost:3001/tasks/:id
router.delete("/:id", (req,res) =>{ 
	// URL parameter values are accessed through the request object's "params" property; the specified property of the object will match the URL parameter name
	taskController.deleteTask(req.params.id).then(result => res.end(result));
})




//update a task
router.put("/:id", (req,res)=>{

	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})



module.exports = router;



// *****************ACTIVITY********
//GETTING AN ITEM
router.get("/:id", (req,res) =>{
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// UPDATING THE STATUS OF AN ITEM
router.patch("/:id/complete", (req,res)=>{

	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})