const mongoose = require("mongoose")

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

module.exports =mongoose.model("Task", taskSchema)
// module.exports - way for js to treat the value as a package that can be used by other files.