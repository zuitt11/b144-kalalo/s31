// contains fuctions and business logic of express.
// CRUD can be used

const Task = require("../models/task")

// controller function:

module.exports.getAllTasks=()=>{
	return Task.find({}).then(result => {
		return result;
	})
}


// controller function for CREATING a task
module.exports.createTask=(requestBody)=>{
	// create a task object based on the mongoose model "Task"
	let newTask = new Task ({
		name: requestBody.name
	})
	//save
	//then method will accept two parameters:
		//first parameter stores the result returned by the mongoose "save"
		// second parameter stores the error object
	return newTask.save().then((task, error) =>{
		if(error) {
			console.log(error);
			// if an error is encountered, the return statement will prevent any other preceeding codes from executing
			// since the following return statement is nested within the then method, chained to the save method, they do not prevent each other from executing the code, which will lead to the else statement not evaluated (that may cause bugs)
			return false;
		}else{
			//if save is successful...
			return task;
		}
	})
}


// delete task
/*
1. look/find for the task with the corresponding id provided in the URL
2. delete the task

*/

module.exports.deleteTask=(taskId)=> {
	// findByIdAndDelete - finds the item to be deleted and removes it from MongoDB; it uses id in looking for the document
	return Task.findByIdAndRemove(taskId).then((removedTask, error) =>{
		if(error) {
			console.log(error);
			return false;
		}else{
			return removedTask;
		}
	})
}


// update task
/*
1. find the id
2. replace the task's name returned from the database with the "name" property from the requestBody
3.	save the task
*/

module.exports.updateTask=(taskId, newContent)=>{
	
	return Task.findById(taskId).then(( result, error ) =>{
		if (error) {
			console.log(error);
			return false;
		}
			//results of findById method will be stored in 'result'
		result.name=newContent.name;

		return result.save().then((updatedTask, error)=>{
			if(error){
				console.log(error);
				return false;
			}else{
				return updatedTask;
			}
		})
	})
}


// *********ACTIVITY**************
// GETTING SPECIFIC ITEM
module.exports.getSpecificTask=(taskId)=>{
	return Task.findById(taskId).then(result => {
		return result;
	})
}


// UPDATING THE STATUS OF AN ITEM
module.exports.updateStatus=(taskId, newStatus)=>{
	
	return Task.findById(taskId).then(( result, error ) =>{
		if (error) {
			console.log(error);
			return false;
		}
		result.status=newStatus.status;

		return result.save().then((updatedStatus, error)=>{
			if(error){
				console.log(error);
				return false;
			}else{
				return updatedStatus;
			}
		})
	})
}
